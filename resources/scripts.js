(function(){
// Anonymous function protects global scope.

// Options for validation.
var validationOptions = {
    insertMessages: true,
    decorateInputElement: true,
    messagesOnModified: true,
    errorElementClass: 'err',
    errorMessageClass: 'help-block',
    debug: true,
    grouping: {
      deep: true,
      observable: false
    }
};

// Domready initalizer for Knockout.
$(document).ready(function(){
  var element = $('#page-application');
  if (element.length) {
    ko.applyBindingsWithValidation(new ViewModel(), element.get(0), validationOptions);
  }
});

// ViewModel for User Management: Users.
function ViewModel() {
  var self = this;

  // namespaces for various functionality
  self.model = {};
  self.data = {};
  self.actions = {};
  self.fields = {};
  self.states = {};
  self.computed = {};
  self.utilities = {};
  self.validation = {};
  self.errors = {};

  setup(self);
}

// Setup all functions, structures and more.
function setup(self) {
  bindActions(self);
  bindStates(self);
  bindData(self);
  bindModel(self);
  bindComputed(self);
  bindUtilities(self);
  bindValidation(self);
  bindErrors(self);


  initialize(self);
}

// Specific actions to run on start up.
function initialize() {
  self.actions.GetUsers();
  self.actions.GetGroups();
}

// Bind observable states.
function bindStates(self) {
  self.states.editing = ko.observable(false);
  self.states.creating = ko.observable(false);
  self.states.grouping = ko.observable(false);
}

// Bind computed values.
function bindComputed(self) {
  self.computed.changing = ko.computed(function(){
    return self.states.editing || self.states.creating;
  });
}

// Bind observable data (essentially, defaults).
function bindData(self) {
  self.data.users = ko.observableArray([]);
  self.data.groups = ko.observableArray([]);
}

// Bind the data model (essentially data to be sent back up).
function bindModel(self) {
  self.model.user = ko.observable(null);
}

// Bind validation configurations.
function bindValidation(self) {
  self.validation.user = function(vm){
    vm.firstName.extend({required: true});
    vm.lastName.extend({required: true});
    vm.userName.extend({required: true});
    vm.email.extend({required: true});
    vm.deaNumber.extend({required: true});

    self.errors.user = ko.validation.group([
      vm.firstName,
      vm.lastName,
      vm.userName,
      vm.email,
      vm.deaNumber
    ]);
  };
}

// Bind validation errors.
function bindErrors(self) {
  self.errors.user = ko.validation.group(self);
}

// Bind actions (essentially, functions that change model state).
function bindActions(self) {

  self.actions.LoadUser = function(user){
    var obj = ko.mapping.toJS(user);
    var vm = ko.mapping.fromJS(obj);
    self.validation.user(vm);
    self.model.user(vm);
  };

  self.actions.EnableEditing = function(){
    self.states.editing(true);
  };

  self.actions.CreateUser = function(){
    self.actions.LoadUser(self.utilities.getNewUser());
    self.states.editing(true);
    self.states.creating(true);
  };

  self.actions.CancelEditor = function(){
    self.model.user(null);
    self.states.editing(false);
    self.states.creating(false);
  };

  self.actions.AddUserGroup = function(){
    self.states.grouping(true);
  };

  self.actions.CancelAddUserGroup = function(){
    self.states.grouping(false);
  };

  self.actions.AddSelectedUserGroup = function(group){
    var duplicate = false;
    self.model.user().groups().forEach(function(item){
      if (item.label() === group.label()) {
        duplicate = true;
      }
    });

    if (duplicate) {
      return;
    }

    self.actions.CancelAddUserGroup();
    self.model.user().groups.push(ko.mapping.fromJS(group));
  };

  self.actions.RemoveSelectedUserGroup = function(group) {
      self.model.user().groups.remove(group);
  };

  self.actions.SaveUser = function(){

    console.log(self.errors.user);
    console.log(self.errors.user());
    console.log(self.errors.user().length);

    if (self.errors.user().length > 0) {
      self.errors.user().forEach(function (observable) {
          if (ko.validation.utils.isValidatable(observable)) {

              if (!observable.isValid()) {
                  observable.isModified(true);
              }
          }
      });
      return;
    }

    var obj = ko.mapping.toJS(self.model.user());
    console.info(obj);
    self.actions.CancelEditor();
  };

  self.actions.GetUsers = function(){
    $.ajax({
      method: 'GET',
      dataType: 'json',
      url: 'endpoints/users.json'
    }).success(function(data){
      ko.mapping.fromJS(data.users, {}, self.data.users);
    });
  };

  self.actions.GetGroups = function(){
    $.ajax({
      method: 'GET',
      dataType: 'json',
      url: 'endpoints/groups.json'
    }).success(function(data){
      ko.mapping.fromJS(data.groups, {}, self.data.groups);
    });
  };

}

// Bind utilities (essentially, helper functions).
function bindUtilities(self) {

  self.utilities.getNewUser = function(){
    return {
        firstName: '',
        lastName: '',
        userName: '',
        email: '',
        deaNumber: '',
        groups: []
    };
  };

}

// Self executing function.
})();
